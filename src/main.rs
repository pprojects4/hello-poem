use std::collections::HashMap;
use std::fs;
use std::sync::Arc;

use poem::error::InternalServerError;
use poem::http::StatusCode;
use poem::listener::TcpListener;
use poem::middleware::{AddData, Tracing};
use poem::session::{CookieConfig, MemoryStorage, ServerSession, Session};
use poem::web::{Data, Form, Path};
use poem::{delete, get, handler, post, EndpointExt, IntoResponse, Response, Route, Server};
use serde::{Deserialize, Serialize};
use sqlx::postgres::PgPoolOptions;
use sqlx::postgres::Postgres;
use sqlx::Pool;
use std::time::Duration;
use tera::{Context, Tera};

struct Res {
    tmp: String,
    ctx: Context,
    status: StatusCode,
    headers: HashMap<String, String>,
}

impl Res {
    fn new(template: String) -> Self {
        Self {
            tmp: template,
            ctx: Context::new(),
            status: StatusCode::OK,
            headers: HashMap::new(),
        }
    }
    fn set_context(mut self, context: Context) -> Self {
        self.ctx = context;
        self
    }
    fn set_status(mut self, status_code: StatusCode) -> Self {
        self.status = status_code;
        self
    }
    fn set_headers(mut self, headers: HashMap<String, String>) -> Self {
        self.headers = headers;
        self
    }
}

struct AppState {
    tmp: Tera,
    db: Pool<Postgres>,
}

impl AppState {
    async fn new() -> Self {
        let db_connection_str = std::env::var("DATABASE_URL")
            .unwrap_or_else(|_| "postgres://postgres:password@localhost/hello_poem".to_string());

        let pool = PgPoolOptions::new()
            .max_connections(5)
            .acquire_timeout(Duration::from_secs(3))
            .connect(&db_connection_str)
            .await
            .expect("can't connect to database");

        let tera = match Tera::new("templates/**/*.*ml") {
            Ok(t) => t,
            Err(e) => {
                println!("Parsing error(s): {}", e);
                ::std::process::exit(1);
            }
        };
        Self {
            tmp: tera,
            db: pool,
        }
    }
    fn render(&self, res: Res) -> poem::Result<impl IntoResponse> {
        match self
            .tmp
            .render(res.tmp.as_str(), &res.ctx)
            .map_err(InternalServerError)
        {
            Err(_) => Ok(Response::builder().status(res.status).finish()),
            Ok(m) => {
                let mut resp = Response::builder();
                for (h, v) in res.headers {
                    resp = resp.header(h, v);
                }
                Ok(resp.status(res.status).body(m))
            }
        }
    }
}

#[derive(Serialize, Deserialize)]
struct Todo {
    id: i32,
    name: String,
    description: Option<String>,
    complete: bool,
}

impl Todo {
    fn new(name: String, description: Option<String>) -> Self {
        Self {
            id: 0,
            name,
            description,
            complete: false,
        }
    }
    async fn get_all(pool: &Pool<Postgres>) -> Result<Vec<Self>, sqlx::Error> {
        sqlx::query_as!(Self, "SELECT * FROM todos;")
            .fetch_all(pool)
            .await
    }
    async fn save(self, pool: &Pool<Postgres>) -> Result<Self, sqlx::Error> {
        sqlx::query_as!(
            Todo,
            "
                INSERT INTO todos(name, description)
                VALUES($1, $2)
                RETURNING *
            ",
            self.name,
            self.description
        )
        .fetch_one(pool)
        .await
    }
    async fn delete(id: i32, pool: &Pool<Postgres>) -> Result<(), sqlx::Error> {
        match sqlx::query!("DELETE FROM todos WHERE id = $1", id)
            .execute(pool)
            .await
        {
            Ok(_) => Ok(()),
            Err(e) => Err(e),
        }
    }
}

#[handler]
fn hello(app: Data<&Arc<AppState>>, session: &Session) -> poem::Result<impl IntoResponse> {
    let count = session.get::<i32>("count").unwrap_or(0) + 1;
    session.set("count", count);
    println!("Count is {}", count);
    app.render(Res::new("hello.html".into()))
}

#[handler]
async fn todos_list_view(app: Data<&Arc<AppState>>) -> poem::Result<impl IntoResponse> {
    let todos = Todo::get_all(&app.db).await.unwrap();
    let mut context = Context::new();
    context.insert("todos", &todos);
    context.insert("form", &CreateTodoForm::new());
    app.render(Res::new("index.html".into()).set_context(context))
}

#[derive(Deserialize, Serialize, Clone)]
struct CreateTodoForm {
    name: Option<String>,
    description: Option<String>,
}

impl CreateTodoForm {
    fn new() -> Self {
        Self {
            name: None,
            description: None,
        }
    }
}

enum TodoError {
    MissingName,
    EmptyName,
}

impl TryInto<Todo> for CreateTodoForm {
    type Error = TodoError;
    fn try_into(self) -> Result<Todo, Self::Error> {
        let name = match self.name {
            None => return Err(TodoError::MissingName),
            Some(n) => n,
        };
        if name.is_empty() {
            return Err(TodoError::EmptyName);
        }
        // any additional validation here
        Ok(Todo::new(name, self.description))
    }
}

#[handler]
async fn create_todo_view(
    app: Data<&Arc<AppState>>,
    raw_form: Form<CreateTodoForm>,
) -> poem::Result<impl IntoResponse> {
    let mut ctx = Context::new();
    let return_form = raw_form.clone();
    let form: Result<Todo, TodoError> = raw_form.0.try_into();
    let new_todo = match form {
        Ok(todo) => match todo.save(&app.db).await {
            Ok(t) => t,
            Err(_e) => {
                ctx.insert("page_errors", &vec!["Failed to save todo".to_string()]);
                ctx.insert("errors", &HashMap::<&str, &str>::new());
                ctx.insert("form", &return_form);
                let res = Res::new("partials/_create_todo.html".into())
                    .set_context(ctx)
                    .set_status(StatusCode::BAD_REQUEST);
                return app.render(res);
            }
        },
        Err(_e) => {
            let mut errors = HashMap::new();
            errors.insert("name", "Name is required");
            ctx.insert("errors", &errors);
            ctx.insert("form", &return_form);
            let todos: Vec<Todo> = vec![];
            ctx.insert("todos", &todos);

            let res = Res::new("partials/_create_todo.html".into())
                .set_context(ctx)
                .set_status(StatusCode::BAD_REQUEST);
            return app.render(res);
        }
    };
    ctx.insert("todos", &vec![&new_todo]);
    ctx.insert("form", &CreateTodoForm::new());
    app.render(Res::new("partials/_create_todo.html".into()).set_context(ctx))
}

#[handler]
async fn delete_todo_view(
    app: Data<&Arc<AppState>>,
    Path(todo_id): Path<i32>,
) -> poem::Result<impl IntoResponse> {
    Todo::delete(todo_id, &app.db).await.unwrap();
    Ok("")
}

#[tokio::main]
async fn main() -> Result<(), std::io::Error> {
    if std::env::var_os("RUST_LOG").is_none() {
        std::env::set_var("RUST_LOG", "poem=debug");
    }
    tracing_subscriber::fmt::init();

    let state = Arc::new(AppState::new().await);

    let app = Route::new()
        .at("/", get(todos_list_view))
        .at("/todo/:todo_id", delete(delete_todo_view))
        .at("/todo/create", post(create_todo_view))
        .at("/hello", get(hello))
        .catch_all_error(|e| async move {
            let template_name = match e.status() {
                StatusCode::NOT_FOUND => "templates/404.html",
                _ => "templates/500.html",
            };
            let msg = fs::read_to_string(template_name).unwrap();
            Response::builder().status(e.status()).body(msg)
        })
        .with(AddData::new(state))
        .with(ServerSession::new(
            CookieConfig::default().name("hello-session"),
            MemoryStorage::new(),
        ))
        .with(Tracing);

    Server::new(TcpListener::bind("0.0.0.0:8000"))
        .run(app)
        .await
}
