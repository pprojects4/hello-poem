-- Add migration script here
-- Create Todo table
CREATE TABLE todos(
  id SERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description TEXT,
  complete BOOL NOT NULL DEFAULT false
);
